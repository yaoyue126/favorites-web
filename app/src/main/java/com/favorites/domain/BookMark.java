package com.favorites.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

public class BookMark {

    private String checksum;
    private RootsBean roots;
    private int version;

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public RootsBean getRoots() {
        return roots;
    }

    public void setRoots(RootsBean roots) {
        this.roots = roots;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public static class RootsBean {

        private BookmarkBarBean bookmark_bar;
        private OtherBean other;
        private SyncedBean synced;

        public BookmarkBarBean getBookmark_bar() {
            return bookmark_bar;
        }

        public void setBookmark_bar(BookmarkBarBean bookmark_bar) {
            this.bookmark_bar = bookmark_bar;
        }

        public OtherBean getOther() {
            return other;
        }

        public void setOther(OtherBean other) {
            this.other = other;
        }

        public SyncedBean getSynced() {
            return synced;
        }

        public void setSynced(SyncedBean synced) {
            this.synced = synced;
        }

        @Data
        @Builder
        @AllArgsConstructor
        @NoArgsConstructor
        public static class BookmarkBarBean {
            private String date_added;
            private String date_modified;
            private String guid;
            private String id;
            private String name;
            private String type;
            private List<Children> children;
        }

        @Data
        @Builder
        @AllArgsConstructor
        @NoArgsConstructor
        public static class OtherBean {
            private String date_added;
            private String date_modified;
            private String guid;
            private String id;
            private String name;
            private String type;
            private List<Children> children;
        }
        @Data
        @Builder
        @AllArgsConstructor
        @NoArgsConstructor
        public static class SyncedBean {
            private String date_added;
            private String date_modified;
            private String guid;
            private String id;
            private String name;
            private String type;
            private List<Children> children;
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Children {
        private String date_added;
        private String guid;
        private String id;
        private String name;
        private String type;
        private String url;

        private List<Children> children;
    }
}

