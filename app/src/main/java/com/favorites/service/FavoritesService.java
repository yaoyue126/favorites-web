package com.favorites.service;

import com.favorites.domain.Collect;
import com.favorites.domain.Favorites;
import com.favorites.domain.FavoritesNodes;

import java.util.List;

public interface FavoritesService {
    public Favorites saveFavorites(Long userId, String name, Long preId);

    public Favorites saveFavorites(Collect collect);

    public void countFavorites(long id);

    public List<FavoritesNodes> getFavoritesList(Long userId);
}
