package com.favorites.utils;

import com.favorites.domain.BookMark;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Type;

public class JsonUtil {
    public static Logger logger = LoggerFactory.getLogger(HtmlUtil.class);

    /**
     * 书签 json返回map
     * @author Jack126
     * @date 2020-12-10
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static BookMark.RootsBean JsonToMap(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[0];
        bytes = new byte[inputStream.available()];
        inputStream.read(bytes);
        String str = new String(bytes);
        Gson gson = new Gson();
        BookMark resultMap = gson.fromJson(str, (Type) BookMark.class);
        return resultMap.getRoots();
    }
}