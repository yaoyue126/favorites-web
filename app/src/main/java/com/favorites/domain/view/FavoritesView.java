package com.favorites.domain.view;

public interface FavoritesView {
    Long getPublicCount();
    Long getId();
    Long getUserId();
    Long getPreId();
    String getName();
    Long getCount();
    Long getCreateTime();
    Long getLastModifyTime();
}
