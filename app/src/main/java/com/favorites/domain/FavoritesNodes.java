package com.favorites.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class FavoritesNodes {
    private Long id;
    private String href;
    private String text;
    private Long[] tags;
    List<FavoritesNodes> nodes;
}
