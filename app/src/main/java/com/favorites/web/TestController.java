package com.favorites.web;

import com.favorites.domain.Favorites;
import com.favorites.domain.view.FavoritesView;
import com.favorites.repository.FavoritesRepository;
import com.favorites.service.FavoritesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class TestController extends BaseController {
    @Resource
    private FavoritesService favoritesService;
    @Autowired
    private FavoritesRepository favoritesRepository;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/test")
    public void getFavorites() {
        Long preId = 1l;
//        List<FavoritesView> list = favoritesService.getFavorites(getUserId(), preId);
//        for (FavoritesView favoritesView : list) {
//            logger.error("\t ==" + favoritesView.toString());
//        }
        Long userId = getUserId();
//        try {
//            Long id = getUserId();
//            if(null != userId && 0 != userId){
//                id = userId;
//            }
//            favorites = favoritesRepository.findByUserIdOrderByLastModifyTimeAsc(id);
//            for (Favorites favorites1:favorites){
//                logger.error("\t=="+favorites1.toString());
//            }
//        } catch (Exception e) {
//            // TODO: handle exception
//            logger.error("getFavorites failed, ", e);
//        }
        List<Favorites> favorites1 = favoritesRepository.findByUserIdAndPreId(userId,preId);
        for (Favorites favorites : favorites1) {
            logger.error("\t==" + favorites.toString());
        }

    }
}
